#GSS Facebook Application

This application allows a survey respondent to login through Facebook for data collection for the GSS survey.  The data is written to a file on
the server in JSON format.  Once the user is done, they are automatically logged out of Facebook and the application is removed from their list
of apps on Facebook.

Questions about this code?  Contact beslow-scott@norc.org
Questions about the GSS survey?  Contact barron-martin@norc.org


##Technology Stack

The app was built with Django using Python and Javascript.  It is run on a Windows server.


##How to run this yourself

####Setting up an application with Facebook

In order to use the Facebook API to collect data, you must set up an application on Facebook.
1. Set up a developer account at https://developers.facebook.com/
2. Create a new app https://developers.facebook.com/docs/facebook-login
..You will receive a Facebook Application ID, which will be needed in future steps
Facebook requires that you choose which pieces of information you would like to coll

####Your Environment

This application requires Python 2.x installed on your machine.  You can then install the other python libraries necessary by running
the command
`pip install -r gss_facebook/requirements.txt`

####Updating the settings

Consistent with Django, the settings are located in settings.py in the gss_facebook directory.  To configure to your own system:

1. Your Django secret key can be set on line 37, as SECRET_KEY

2. The App ID that you retrieved from Facebook is set to the variable FACEBOOK_APP_ID on line 36

3. On line 29, we are setting Debug to False for the production machine, based on our own machine name.  You should update this if you ever want Debug to true (
which is should be on production machines!)

4. On line 174, we have configured the output directory for the JSON files.  You should set that to where you want your files to be, and make sure that
the directory already exists.

Also, in templates/login.html. you'll find the Facebook Login button.  You'll need to set the 'scope' to whatever attributes you are retrieving about the respondent.

####Building the application
Our application is built on a Windows server using IIS.  To build on IIS, you can simply run the file gss_facebook/gss_facebook/build/install.iss.
Before doing so, update your URL, workspace, and the output directory.  When completed, it will have created a .exe file, that can be run from a server
that is running IIS.


##FAQ
**1. Why is there a Close button that doesn't seem to work on Chrome, Firefox, or Safari?**

The close button allows the user to close the window progmattically.  Most browsers don't let an application do this unless the application alsoThe
opened the window.  Internet Explorer does let you do this, and since the majority of our surveys are conducted on Internet Explorer, we wanted to provide
any easy way for our interviews to control their windows. 

**2. Why are you using the Javascript SDK rather than doing Facebook login from the server side?**

Our design decisions here were based on having Facebook control the whole login process.  You'll notice that when you run the application,
you are directed to Facebook's own interface.  This was our preferred method based on privacy concerns.

**3. Why didn't you use the python-facebook libray?**

Initially we did.  However, unfortunately it did some stuff under the covers which were against our own security protocol at NORC.  So, instead
we switched to using Facebook's RESTful API, which was a lot less elegant, but a little bit more under our control.