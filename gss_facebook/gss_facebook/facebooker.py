import logging
import requests

# The python-facebook library was initially used, but has since been abandoned due to permissions issues with
# the way the production server was configured.  The method grab_info_from_facebook uses it, and is therefore
# commented out as well.  It is being left in here for now, in the case that we would like to ultimately
# consider using it again

logger = logging.getLogger(__name__)

# The fields that we are looking to grab
fields = ['events', 'likes', 'books', 'fitness.runs', 'music', 'videos', 'news.reads']


# Uses Facebook's REST GRAPH API in order to get data from Facebook
def grab_info_from_facebook_rest(access_token, fb_user_id):
    try:
        url = 'https://graph.facebook.com/v2.5/' + fb_user_id + \
              '?fields=first_name,last_name,age_range,link,gender,locale,timezone,updated_time,verified,birthday,education,relationship_status,significant_other,political,religion,location&access_token=' + access_token
        results = requests.get(url).json()
    except Exception as e:
        logger.exception(e)
        return {"error": e.message}

    if "error" in results:
        return {"error": results["error"]["message"]}
    ret_val = {'profile': results}

    for field in fields:
        url = 'https://graph.facebook.com/v2.5/%s/%s?access_token=%s' % (fb_user_id,field,access_token)

        try:
            results = requests.get(url).json()
            info = results["data"]
        except Exception as e:
            logger.exception(e)
            return {"error": e.message}

        if "error" in results:
            return {"error": results["error"]["message"]}

        if "paging" in results and "next" in results["paging"] and results["paging"]["next"]:
            additional_results = paginate(results["paging"]["next"])
            info.extend(additional_results)

        ret_val[field] = info

    return convert_unicode_to_ascii(ret_val)


# Grab a specific field from the Facebook Graph API
def grab_field(graph, field):
    try:
        results = graph.get_connections(id='me', connection_name=field)
        ret_val = results["data"]
    except Exception as e:
        logger.error(e)
        return {"error": e.message}

    if "paging" in results and "next" in results["paging"] and results["paging"]["next"]:
        additional_results = paginate(results["paging"]["next"])
        ret_val.extend(additional_results)

    return ret_val


# Handles pagination, if the field requested requires it.
def paginate(starting_url):
    data = requests.get(starting_url).json()
    ret_val = data["data"]
    while "paging" in data and "next" in data["paging"] and data["paging"]["next"]:
        data = requests.get(data["paging"]["next"]).json()
        ret_val.extend(data["data"])

    return ret_val


# This helper method converts a unicode string to ascii
def convert_unicode_to_ascii(unicode_string):
    if isinstance(unicode_string, dict):
        return {convert_unicode_to_ascii(key): convert_unicode_to_ascii(value) for key, value in
                unicode_string.iteritems()}
    elif isinstance(unicode_string, list):
        return [convert_unicode_to_ascii(element) for element in unicode_string]
    elif isinstance(unicode_string, unicode):
        return unicode_string.encode('utf-8')
    else:
        return unicode_string
