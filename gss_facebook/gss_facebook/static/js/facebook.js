/*
This file does most of the Facebook logging in and loggin out magic.  It was initially copied from
https://developers.facebook.com/docs/facebook-login/web

and then was modified to suit our needs
 */

// This is called with the results from from FB.getLoginStatus().
function statusChangeCallback(response) {

    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
        // Logged into your app and Facebook.
        if (global.logout) {
            // Right here we want to logout and remove app permissions for the user.  You can't logout
            // if you don't have permissions, and you can't remove permissions if you are logged out, so
            // we do them at the same time.  This seems to work 99% of the time, but throw error messages
            // to alert the user if not.
            FB.logout(function (response) {
                if (response.status != "unknown") {
                    alert("Unable to sign-out from Facebook.  Please do this manually");
                }
            });
            FB.api('/me/permissions', 'delete', function (response) {
                if (response.success != true) {
                    alert("Unable to remove this app from your Facebook list of apps.  Please do this manually");
                }
            });
        }
        else {
            startCollection();
        }
    } else if (response.status === 'not_authorized') {
        // The person is logged into Facebook, but not your app.
        document.getElementById('status').innerHTML = 'Please log ' +
            'into this app.';
    } else {
        // The person is not logged into Facebook, so we're not sure if
        // they are logged into this app or not.
        document.getElementById('status').innerHTML = 'Please log ' +
            'into Facebook.';
    }
}

// This function is called when someone finishes with the Login
// Button.  See the onlogin handler attached to it in the sample
// code below.
function checkLoginState() {
    FB.getLoginStatus(function (response) {
        statusChangeCallback(response);
    });
}

function loginBtnPush() {
    FB.getLoginStatus(function (response) {
        if (response.status === 'connected') {
            startCollection();
        }
        else {
            FB.login(function (response) {
                if (response.authResponse) {
                    startCollection();
                } else {
                    alert("Login failed");
                }
            });


        }
    });
}

window.fbAsyncInit = function () {
    FB.init({
        appId: global.appId,
        cookie: true,  // enable cookies to allow the server to access
                       // the session
        xfbml: true,  // parse social plugins on this page
        version: 'v2.2' // use version 2.2
    });

    // Now that we've initialized the JavaScript SDK, we call
    // FB.getLoginStatus().  This function gets the state of the
    // person visiting this page and can return one of three states to
    // the callback you provide.  They can be:
    //
    // 1. Logged into your app ('connected')
    // 2. Logged into Facebook, but not your app ('not_authorized')
    // 3. Not logged into Facebook and can't tell if they are logged into
    //    your app or not.
    //
    // These three cases are handled in the callback function.

    FB.getLoginStatus(function (response) {
        statusChangeCallback(response);
    });

};

// Load the SDK asynchronously
(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

function startCollection() {

    $("#facebookBtnDiv").hide();
    $("#waitSpin").show();

    $.ajax({
        type: "POST",
        url: "/collect-data/",
        data: {
            user_id: global.userId,
            access_token: FB.getAccessToken(),
            fb_user_id: FB.getUserID(),
            csrfmiddlewaretoken: global.csrf_token
        },
        success: function (msg) {
            if (msg.response != "success") {
                alert("ERROR: " + msg.responseData);
                window.location.href = '/'
                return;
            }
            window.location.href = "/done";

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("ERROR: " + errorThrown);
        }
    });
}