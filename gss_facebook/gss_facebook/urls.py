"""gss_facebook URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from . import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls import patterns

urlpatterns = i18n_patterns(
    url(r'^admin/', include(admin.site.urls)),
    url(r'^done', views.done),
    url(r'^privacy', views.privacy),
    url(r'^channelUrl', views.channel_url),
    url(r'^login/(?P<user_code>.+)$', views.login),
    url(r'^(?P<user_code>.+)$', views.confirm_privacy),
    url(r'^', views.enter_code),
)

urlpatterns += patterns('',
    (r'^i18n/', include('django.conf.urls.i18n')),
    (r'^collect-data/', views.collect_data),
)

urlpatterns += staticfiles_urlpatterns()
