import json

from django.http import JsonResponse
from django.shortcuts import render_to_response
import logging

from django.template import RequestContext
from os.path import exists
from gss_facebook import settings
from gss_facebook.facebooker import grab_info_from_facebook_rest

logger = logging.getLogger(__name__)


# When a user goes to the website, this is the first view.  It prompts the user to enter their GSS-provided code
def enter_code(request):
    data = {"facebook_app_id": settings.FACEBOOK_APP_ID}
    return render_to_response("enter_code.html", data, context_instance=RequestContext(request))


# Show the user the privacy policy, and ask him/her to accept it
def confirm_privacy(request, user_code):

    # Only proceed if the user has given a valid code, which are two string separated by a dash
    if "-" not in user_code:
        return enter_code(request)
    if user_code.endswith('/'):
        user_code = user_code[:-1]

    data = {"facebook_app_id": settings.FACEBOOK_APP_ID, "user_code": user_code}
    return render_to_response("confirm_privacy.html", data, context_instance=RequestContext(request))


# Prompt user to login through Facebook
def login(request, user_code):

    # Only proceed if the user has given a valid code, which are two string separated by a dash
    if "-" not in user_code:
        return enter_code(request)
    if user_code.endswith('/'):
        user_code = user_code[:-1]
    data = {"facebook_app_id": settings.FACEBOOK_APP_ID, "user_code": user_code}
    return render_to_response("login.html", data, context_instance=RequestContext(request))


# This view handles the collection of data from Facebook, and writing to a file.  It is called via an AJAX call,
# so it returns a JSON response
def collect_data(request):

    user_id = request.POST.get("user_id")
    access_token = request.POST.get("access_token")
    fb_user_id = request.POST.get("fb_user_id")

    if user_id is None or access_token is None:
        return JsonResponse({
            "responseData": "Unable to resolve user id or access token",
            "response": "failure"
        }, safe=False)

    # Use the Facebook ID of the user and our app's access token to get a bunch of info from Facebook
    collected_fb_data = grab_info_from_facebook_rest(access_token, fb_user_id)
    if "error" in collected_fb_data:
        return JsonResponse({
            "responseData": collected_fb_data["error"],
            "response": "failure"
        }, safe=False)

    user_data = {"user": user_id.encode('utf-8'), "facebook_data": collected_fb_data}

    # Write the data to a file in pretty JSON format
    try:
        data_output_dir = settings.DATA_OUTPUT_DIR
        logger.info("Writing data to " + data_output_dir)

        file_name = data_output_dir + user_id + ".json"

        # If this file exists, add a sequential numeral to the end of it.
        counter = 1
        while exists(file_name):
            counter += 1
            file_name = data_output_dir + user_id + "-" + str(counter) + ".json"

        output_file = open(file_name, 'w')
        output_file.write(json.dumps(user_data, indent=4))

    except Exception as e:
        return JsonResponse({
            "responseData": e.message,
            "response": "failure"
        }, safe=False)

    print "Done, success"
    return JsonResponse({
        "responseData": "",
        "response": "success"
    }, safe=False)


# This view shows the user a screen that says they are done
def done(request):
    data = {"facebook_app_id": settings.FACEBOOK_APP_ID}
    return render_to_response("done.html", data, context_instance=RequestContext(request))


# Show our privacy policy
def privacy(request):
    return render_to_response("privacy.html", context_instance=RequestContext(request))


# ChannelUrl is there for Facebook.  Read more about it here:
# http://stackoverflow.com/questions/15821196/channel-url-facebook
def channel_url(request):
    return render_to_response("channelUrl.html")
