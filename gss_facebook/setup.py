import os, subprocess, shutil, traceback

import time

VIRTUALENV_NAME = 'venv'
DJANGO_PROJECT_NAME = 'gss_facebook'
# PROJECT_DIR = os.path.dirname(__file__)
# print PROJECT_DIR

PROJECT_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + "\\..\\"
# PROJECT_DIR = "D:/projects/gotrack"
# PROJECT_DIR = 'D:\workspace\gss_facebook'
PROJECT_DIR = 'D:\Projects\GSSFaceBook'
print "PROJECT_DIR: " + PROJECT_DIR

logfile = open(os.path.join(PROJECT_DIR, "log", "setup.txt"), "w")
errfile = open(os.path.join(PROJECT_DIR, "log", "setup_errors.txt"), "w")

subprocess.call(["pip", "install", "virtualenv"], stdout=logfile, stderr=errfile)
print "Setting venv"
if not os.path.exists(VIRTUALENV_NAME):
    print "Creating virtual environment"
    subprocess.call(["virtualenv", "venv"], stdout=logfile, stderr=errfile)

subprocess.call([os.path.join(PROJECT_DIR, "venv", "Scripts", "pip.exe"), "install", "--upgrade", "pip"], stdout=logfile, stderr=errfile)
subprocess.call([os.path.join(PROJECT_DIR, "venv", "Scripts", "pip.exe"), "install", "Django==1.8.6"], stdout=logfile, stderr=errfile)
subprocess.call([os.path.join(PROJECT_DIR, "venv", "Scripts", "pip.exe"), "install", "facebook-sdk==0.4.0"], stdout=logfile, stderr=errfile)
subprocess.call([os.path.join(PROJECT_DIR, "venv", "Scripts", "pip.exe"), "install", "requests==2.8.1"], stdout=logfile, stderr=errfile)

try:
    shutil.copy(os.path.join(PROJECT_DIR, "zoofcgi.py"), os.path.join("C:\\", "Zoo", "Workers", "python", "zoofcgi.py"))
    logfile.write("Successfully copied and replaced system zoofcgi.py file")
except Exception as e:
    errfile.write(traceback.format_exc())

subprocess.call([os.path.join(PROJECT_DIR, "venv", "Scripts", "python.exe"), "manage.py", "collectstatic", "--noinput"], stdout=logfile, stderr=errfile)
try:
    shutil.rmtree(os.path.join(PROJECT_DIR, "gss_facebook", "static"))
except Exception as e:
    errfile.write(traceback.format_exc())

logfile.close()
errfile.close()

print "Done!  Check 'log/setup.txt' and 'log/setup_errors.txt' to see the output."
time.sleep(5)
